package primo.musicdb.model.entity.enums;

public enum GenreName {
    POP, ROCK, METAL, OTHER
}
