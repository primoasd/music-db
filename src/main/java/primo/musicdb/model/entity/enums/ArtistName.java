package primo.musicdb.model.entity.enums;

public enum ArtistName {
    QUEEN, METALLICA, MADONNA
}
