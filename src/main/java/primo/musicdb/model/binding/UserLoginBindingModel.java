package primo.musicdb.model.binding;

import javax.validation.constraints.*;

public class UserLoginBindingModel {
    private String username;
    private String password;

    public UserLoginBindingModel() {
    }

    @NotNull(message = "The username can not be null")
    @NotBlank(message = "The username can not be blank")
    @NotEmpty(message = "The username can not be empty")
    @Size(min = 3, max = 20, message = "The username cannot be less than 3 characters nor more than 20 characters")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @NotNull(message = "The password can not be null")
    @NotBlank(message = "The password can not be blank")
    @NotEmpty(message = "The password can not be empty")
    @Size(min = 5, max = 20, message = "The password cannot be less than 5 characters nor more than 20 characters")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
