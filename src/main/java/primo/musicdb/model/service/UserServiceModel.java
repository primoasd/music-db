package primo.musicdb.model.service;

import javax.validation.constraints.*;

public class UserServiceModel {
    private String username;
    private String email;
    private String fullName;
    private String password;

    public UserServiceModel() {
    }

    @NotNull(message = "The username can not be null")
    @NotBlank(message = "The username can not be blank")
    @NotEmpty(message = "The username can not be empty")
    @Size(min = 3, max = 20, message = "The username cannot be less than 3 characters nor more than 20 characters")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @NotNull(message = "The full name can not be null")
    @NotBlank(message = "The full name can not be blank")
    @NotEmpty(message = "The full name can not be empty")
    @Size(min = 3, max = 20, message = "The full name cannot be less than 3 characters nor more than 20 characters")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Email(message = "The email must be valid")
    @NotNull(message = "The email can not be null")
    @NotBlank(message = "The email can not be blank")
    @NotEmpty(message = "The email can not be empty")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotNull(message = "The password can not be null")
    @NotBlank(message = "The password can not be blank")
    @NotEmpty(message = "The password can not be empty")
    @Size(min = 5, max = 20, message = "The password cannot be less than 5 characters nor more than 20 characters")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
