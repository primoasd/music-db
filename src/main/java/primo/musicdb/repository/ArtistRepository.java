package primo.musicdb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import primo.musicdb.model.entity.Artist;

public interface ArtistRepository extends JpaRepository<Artist, String> {
}
