package primo.musicdb.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import primo.musicdb.model.service.UserServiceModel;
import primo.musicdb.repository.UserRepository;
import primo.musicdb.service.UserService;

import primo.musicdb.model.entity.User;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository repository;
    private final ModelMapper modelMapper;

    @Autowired
    public UserServiceImpl(UserRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    @Override
    public void register(UserServiceModel userServiceModel) {
        this.repository.saveAndFlush(
                this.modelMapper.map(
                        userServiceModel, User.class
                )
        );
    }

    @Override
    public User findByUser(String username) {
        return
                this.repository
                        .findByUsername(username);
    }
}
