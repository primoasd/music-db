package primo.musicdb.service;

import primo.musicdb.model.entity.User;
import primo.musicdb.model.service.UserServiceModel;

public interface UserService {
    void register(UserServiceModel userServiceModel);

    User findByUser(String username);
}
