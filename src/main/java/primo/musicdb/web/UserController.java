package primo.musicdb.web;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import primo.musicdb.model.binding.UserLoginBindingModel;
import primo.musicdb.model.binding.UserRegisterBindingModel;
import primo.musicdb.model.service.UserServiceModel;
import primo.musicdb.service.UserService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final ModelMapper modelMapper;

    @Autowired
    public UserController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/register")
    public String getRegister(Model model) {
        if (!model.containsAttribute("userRegisterBindingModel")) {
            model.addAttribute("userRegisterBindingModel", new UserRegisterBindingModel());
        }
        return "register";
    }

    @PostMapping("/register")
    public String postRegister(@Valid @ModelAttribute("userRegisterBindingModel")
                                       UserRegisterBindingModel userRegisterBindingModel,
                               BindingResult bindingResult, HttpSession session,
                               RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors() ||
                !userRegisterBindingModel
                        .getPassword()
                        .equals(userRegisterBindingModel
                                .getConfirmPassword())) {

            redirectAttributes
                    .addFlashAttribute("userRegisterBindingModel"
                            , userRegisterBindingModel);

            redirectAttributes
                    .addFlashAttribute("org.springframework.validation.BindingResult.userRegisterBindingModel"
                            , bindingResult);

            return "redirect:register";
        }

        this.userService.register(
                this.modelMapper.map(
                        userRegisterBindingModel, UserServiceModel.class
                )
        );

        session.setAttribute("user"
                , userRegisterBindingModel.getUsername());

        return "redirect:/";
    }

    @GetMapping("/login")
    public String getLogin(Model model) {
        if (!model.containsAttribute("userLoginBindingModel")) {
            model.addAttribute("userLoginBindingModel", new UserLoginBindingModel());
        }
        return "login";
    }


    @PostMapping("/login")
    public String postLogin(@Valid @ModelAttribute("userLoginBindingModel")
                                    UserLoginBindingModel userLoginBindingModel,
                            BindingResult bindingResult
            , HttpSession session
            , RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {

            redirectAttributes
                    .addFlashAttribute("userLoginBindingModel"
                            , userLoginBindingModel);

            redirectAttributes
                    .addFlashAttribute("org.springframework.validation.BindingResult.userLoginBindingModel"
                            , bindingResult);

            return "redirect:login";
        }

        if (this
                .userService
                    .findByUser(userLoginBindingModel
                        .getUsername()) == null) {

            redirectAttributes.addFlashAttribute("notFound", true);

            return "redirect:login";
        }

        session.setAttribute("user"
                , userLoginBindingModel.getUsername());


        return "redirect:/";
    }

    @GetMapping("/logout")
    public String getLogout(HttpSession session) {

        if (session.getAttribute("user") == null) {
            return "redirect:/";
        }

        session.invalidate();
        return "redirect:/";
    }
}
