package primo.musicdb.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import primo.musicdb.service.ArtistService;

@Component
public class DataInit implements CommandLineRunner {
    private final ArtistService artistService;

    @Autowired
    public DataInit(ArtistService artistService) {
        this.artistService = artistService;
    }

    @Override
    public void run(String... args) throws Exception {
        artistService.initArtists();
    }
}
